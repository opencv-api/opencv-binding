FROM amydoxym/opencv-nodejs:node9-opencv3.4.1-contrib

WORKDIR /app/pfe

COPY ./package.json /app/pfe/package.json
RUN npm install -g nodemon && npm install
COPY ./package-lock.json /app/pfe/package-lock.json
COPY ./src /app/pfe/src

CMD ["nodemon", "-L", "./src/bin/www"]
