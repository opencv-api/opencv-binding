var express = require('express');
var services = require('../services');
var router = express.Router();


const needImageEnBase64 = (request, res, next) => {
  const { imgBase64 } = request.body;
  if (typeof imgBase64 !== 'string') {
    return res.status(404).send('Image data should be the base64 string of the image');
  }
  request.params.img = services.convertirDuBase64(imgBase64);
  return next();
}
router.get('/', (req, res) => {
  res.render('faces');
});
router.post('/', needImageEnBase64,function(req, res, next) {
  var reqParams = req.params;
  const faceDetector = new services.FaceAndEyesDetector(reqParams.img, reqParams.faceDetectionAlgo, reqParams.eyeDetectionAlgo);
  const facesImg = faceDetector.detectFacesAndEyes();
  const facesImgBase64 = services.convertirJpgBase64(facesImg);
  res.status(202).send({ base64Data: facesImgBase64 });
  //res.render('/');
})
module.exports = router;
