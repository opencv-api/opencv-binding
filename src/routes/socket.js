var services = require('../services');
// camera properties
var camWidth = 320;
var camHeight = 240;
var camFps = 10;
var camInterval = 1000 / camFps;

// face detection properties
var rectColor = [0, 255, 0];
var rectThickness = 2;



module.exports = function (socket) {
  // // initialize camera

  socket.on('imageFromClient',function(data){
    var img = services.convertirDuBase64(data.image);
    const faceDetector = new services.FaceAndEyesDetector(img, data.faceDetectionAlgo, data.eyeDetectionAlgo);
    const facesImg = faceDetector.detectFacesAndEyes();
    const facesImgBase64 = services.convertirJpgBase64(facesImg);
    //console.log(facesImgBase64);
    socket.emit('imageFromServer', {im: facesImgBase64});
    //console.log(data.buffer);
  });

  socket.on('recognize', function (data){
    var img = services.convertirDuBase64(data.img);

    const faceRecognizer = new services.FaceRecognizer();
    const facesImg = faceRecognizer.recognizeFromImage(img);
    const facesImgBase64 = services.convertirJpgBase64(facesImg);
    //console.log(facesImgBase64);

    socket.emit('recognized', {img: facesImgBase64});
  });

};
