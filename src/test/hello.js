var expect = require('chai').expect;
var operation= require('../build/Release/addon');
describe("Addition", function() {
  describe("Addition of two numbers", function() {
    it("adds two numerical value", function() {
      var intOp = operation.add(5,19);
      var doublOp = operation.add(45.2, 78.3);

      expect(intOp).to.deep.equal(24);
      expect(doublOp).to.deep.equal(123.5);
    });
  });

});
