var socket = io.connect('ws://localhost:45000', {transports: ['websocket']});
var webCamVideo = $('#webcam-video')[0];
var canvas = $('#canvas-video')[0];
var canvasOutput = $('#canvas-output')[0];
var context = canvas.getContext('2d');
var contextOutput = canvasOutput.getContext('2d');
let streamWebCam;


   //socket.emit('webcam');
   context.fillStyle = '#333';
   context.fillText('Loading...', canvas.width/2-30, canvas.height/3);
   contextOutput.fillStyle = '#333';
   contextOutput.fillText('Loading...', canvasOutput.width/2-30, canvasOutput.height/3);

function startWebCam() {
  navigator.getUserMedia = navigator.getUserMedia ||navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.oGetUserMedia ||  navigator.msGetUserMedia;
  if (navigator.getUserMedia) {
    navigator.getUserMedia({video:true}, streamWebCamera, throwErr);
  }
}

function streamWebCamera(stream) {
  streamWebCam = stream;
  webCamVideo.src = window.URL.createObjectURL(streamWebCam);
 webCamVideo.play();

}
function throwErr(e) {
  alert(e.name);
}
function snap(){
  canvas.width = webCamVideo.clientWidth;
  canvas.height = webCamVideo.clientHeight;
  context.drawImage(webCamVideo,0, 0 )

}
function stopWebcam() {
  var track = streamWebCam.getTracks()[0];
  track.stop();
}
function detectFacesInWebCam(){
  snap();
  var dataURL = canvas.toDataURL('image/jpeg', 1.0);
  //$('#canvasImg').src = dataURL;
  socket.emit('imageFromClient', {image: dataURL, faceDetectionAlgo: "", eyeDetectionAlgo: ""});

}
socket.on('imageFromServer', function(data){
  var img = new Image();
  img.onload = function () {
    canvasOutput.width = webCamVideo.clientWidth;
    canvasOutput.height = webCamVideo.clientHeight;
    contextOutput.drawImage(this, 0, 0, canvasOutput.width, canvasOutput.height);
  };

  img.src =  'data:image/jpeg;base64,' + data.im;

});
