var dimensionMax =  500;

/**
  Function scaleFactor
  @param: an image from which the scale will be made
  This function shrinks the image provided if the image is greater than the maximal dimension which is setted to 500
  If the width is greater than the height, the greater will be used
*/
function scaleFactor(imgInput){
  var imgWidth = imgInput.width;
  var imgHeight = imgInput.height;

  var scale = 1.0;
  if(imgWidth > imgHeight ){
    scale = dimensionMax /imgWidth;
  } else{
    scale = dimensionMax /imgHeight;
  }

  if (scale < 1.0) {
    imgWidth = imgWidth * scale;
    imgHeight = imgHeight * scale;
  }
  return({width: imgWidth, height: imgHeight});
}

/**
  Function afficheImage
  @param: the id pointing to the img tag in the document, and the details on the imageElement
  It will display the image and shrinks it if it is too large to fit in the appropriate containter
*/
function afficheImage(id, imgData){
  var imageElement = document.getElementById(id);
  var scaledImg = new Image();
  scaledImg.onload = function(evnt) {
    var newImage = evnt.target;
    var scalingDim = scaleFactor(newImage);
    imageElement.src = imgData;
    imageElement.width = scalingDim.width;
    imageElement.height = scalingDim.height;
  };
  scaledImg.src = imgData;

}
