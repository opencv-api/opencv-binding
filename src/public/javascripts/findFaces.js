var socket1 = io.connect('ws://localhost:45000', {transports: ['websocket']});
var uploadedFile;
var uploadedFileData;
let faceDetectionAlgo;
let eyeDetectionAlgo;
var errorMsg = $('#errorMsg');

var canvasOut = $('#canvas-recognized')[0];
var contextOut = canvasOut.getContext('2d');


contextOut.fillStyle = '#333';
contextOut.fillText('Loading...', canvasOut.width/2-30, canvasOut.height/3);



//Listen to radio buttons change
$('input[type="radio"]').on('click change', function(e) {
  switch (e.target.name) {
    case "faceClassifier":
    faceDetectionAlgo = e.target.value;
    console.log(faceDetectionAlgo);
    break;
    case "eyeClassifier":
    eyeDetectionAlgo = e.target.value;
    console.log(eyeDetectionAlgo);
    break;
    default:
    console.log(e.target);
  }

});


function onImgUploaded(evnt) {

  uploadedFile = evnt.target.files[0];
  var fileReader = new FileReader();
  fileReader.onload = function(src) {
    uploadedFileData = src.target.result;
    afficheImage('inputFaceImg', uploadedFileData);
    afficheImage('outputFaceImg', uploadedFileData);
  };
  fileReader.readAsDataURL(uploadedFile);
}

function requestDection(imgData, algorithms, url){

  sendXhr(imgData,algorithms,  url, function(error, respImgData){
    if (error) {
      var errorMsg = $('#errorMsg');
      errorMsg.html(
        "<div class=\"alert alert-danger alert-dismissible\">\
        <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\
        <strong>Error! </strong><span>"+error+"</span>\
        </div>"
      );
    }else {
      afficheImage('outputFaceImg', respImgData);
    }
  });
}
function findFaces(){
  console.log("We are using the '"+faceDetectionAlgo+"' algorithm to detect faces");
  const algorithms = {
    "faceDetectionAlgo": faceDetectionAlgo,
    "eyeDetectionAlgo": eyeDetectionAlgo
  };
  return requestDection(uploadedFileData,algorithms,  '');
}

function onExtract(){
  console.log("Recognize me");
  socket1.emit('recognize', {img: uploadedFileData});



  // });
}
  socket1.on("recognized", function(data){
    console.log(data.img);
    console.log("hello");
      var img = new Image();
      img.onload = function () {
        canvasOut.width = 500;
        canvasOut.height =500;
        contextOut.drawImage(this, 0, 0, canvasOut.width, canvasOut.height);
      };

      img.src =  'data:image/jpeg;base64,' + data.img;
  });
