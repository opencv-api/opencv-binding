function getURI(url){ //Here the url designed the endpoint
  return location.href + url;
}

function sendXhr (imgData,algorithms, url, onSuccess){

  var httpRequest = new XMLHttpRequest();

  httpRequest.onreadystatechange = function(error) {
    if(httpRequest.readyState == 4) { //Status: Pret
      if (httpRequest.status == 202) { // Status 202 = Accepted
        const dataEnBase64 = JSON.parse(httpRequest.responseText).base64Data;
        onSuccess(null, 'data:image/jpeg;base64,' + dataEnBase64);
      }else {
        onSuccess(httpRequest.status + ': ' + httpRequest.responseText, null);
      }
    }
  };
  httpRequest.open('POST', getURI(url), true );
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.send(JSON.stringify({
    "imgBase64": imgData,
     "faceDetectionAlgo": algorithms.faceDetectionAlgo,
     "eyeDetectionAlgo": algorithms.eyeDetectionAlgo
  }));

}
