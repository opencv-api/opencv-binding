var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var i18n = require("i18n-express");

var indexRoutes = require('./routes/index');
var usersRoutes = require('./routes/users');
var facesRoutes = require('./routes/faces');
var webCamRoutes = require('./routes/webcamDetection');
var documentationRoutes = require('./routes/documentation');
//C++ addons exposed to the app as services
var services = require('./services')
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(favicon(path.join(__dirname, 'public/images', 'favicon.jpg')));
app.use(logger('dev'));
app.use(bodyParser.json({limit:'50mb'}));
app.use(bodyParser.urlencoded({ limit:'50mb', extended: true }));
app.use(cookieParser());
//Translation module
app.use(i18n({
    translationsPath: path.join(__dirname, 'public','i18n'),
    siteLangs: ["en", "fr"],
    textsVarName: 'translation'
}));

app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRoutes);
app.use('/users', usersRoutes);
app.use('/faces', facesRoutes);

app.use('/webcam', webCamRoutes);




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
