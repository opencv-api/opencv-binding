'use strict';
const  portWebCam = 0;
const classifier = new CvClassifier("haarFrontalFaceAlt2").getClassifier();
const { runVideoFaceDetection } = require('./commons');

function detectFaces (image) {
  const options = {
    minSize: new opencv.Size(100, 100),
    scaleFactor: 1.2,
    minNeighbors: 10
  };
  return classifier.detectMultiScale(image.bgrToGray(), options).objects;
};

runVideoFaceDetection(portWebCam, detectFaces);
