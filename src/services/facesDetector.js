'use strict';

/**
FaceDetector class.

@constructor
@param {String} image - The image on which face detection will be perfomed
@param {String} faceDetectionAlgorithm - The algorithm that will be used during the face detection process
*/
function FaceDetector(image, faceDetectionAlgorithm){
  this.image = image;
  this.faceDetectionAlgorithm = faceDetectionAlgorithm;
}

/**
  @return {Object} location of faces detected
*/
FaceDetector.prototype.getFacesLocation = function () {
  const classificateur = new CvClassifier(this.faceDetectionAlgorithm).getClassifier();

  console.log(this.faceDetectionAlgorithm);
  const facesResults = classificateur.detectMultiScale(this.image.bgrToGray());

  return facesResults;
};

/**
Detect faces
@return {Object} image
*/
FaceDetector.prototype.detectFaces = function () {
  const detector = this.getFacesLocation();
  if(!detector.objects.length) {
    console.log("0 faces detected");
    return this.image;
  }
  /**
  If faces have been detected in the image, we draw a rectangle on faces
  Else we return the input image as it was
  We use the confidence the detector  tp
  */
  const sortByNumDetections = result => result.numDetections
  .map((num, idx) => ({ num, idx }))
  .sort(((n0, n1) => n1.num - n0.num))
  .map(({ idx }) => idx);

  console.log('faceRects', detector.objects);
  console.log('confidence', detector.numDetections);

  const facesImg = this.image.copy();
  const faces = detector.objects;
  faces.forEach((faceDetected,i) => {
    const rectangleColor = new opencv.Vec(100,0,113);
    const rectangleEpaisseur =  numDetections[i] < 10 ? 1 : 4;
    facesImg.drawRectangle(
      new opencv.Point(faceDetected.x, faceDetected.y),
      new opencv.Point(faceDetected.x + faceDetected.width, faceDetected.y+ faceDetected.height),
      { rectangleColor, rectangleEpaisseur }
    );
  });
  return facesImg
};

FaceDetector.prototype.getFaceImage = function () {
  const faceRects = this.getFacesLocation();
  return this.image.getRegion(faceRects[i]);
};

module.exports = FaceDetector
