'use strict';

/**
Detect Face and eyes
@constructor
@param {String} image - The image on which Detection will be performed
@param {String} faceDetectionAlgorithm - Algorithm to use for detecting Faces
@param {String} eyeDetectionAlgorithm - Algorithm to use for detecting eyes in the face
*/
function FaceAndEyesDetector(image, faceDetectionAlgorithm, eyeDetectionAlgorithm){
  this.image = image;
  this.faceDetectionAlgorithm = faceDetectionAlgorithm;
}

/**
  Get location of eyes in a face
  @param {Region} faceLocation - Coordinates of the face
*/
FaceAndEyesDetector.prototype.getEyesLocation= function (faceLocation)  {
  const classificateurYeux = new opencv.CascadeClassifier(opencv.HAAR_EYE);
  const faceRegion = this.image.getRegion(faceLocation);
  const eyeResult = classificateurYeux.detectMultiScale(faceRegion);
  console.log('eyeRects:', eyeResult.objects);
  console.log('confidences:', eyeResult.numDetections);
  return eyeResult;
};
/**
  Detect Faces and eyes
  @return {Object} - image with face and eyes drawn on it if detected
*/
FaceAndEyesDetector.prototype.detectFacesAndEyes = function () {
  const facesDetector = new FaceDetector(this.image, this.faceDetectionAlgorithm);
  const classificateurYeux= new CvClassifier(this.eyeDetectionAlgorithm).getClassifier();
  const faceRects = facesDetector.getFacesLocation();

  if (!faceRects.objects.length) {
    console.log('0 faces detected!');
  }

  const sortByNumDetections = result => result.numDetections
  .map((num, idx) => ({ num, idx }))
  .sort(((n0, n1) => n1.num - n0.num))
  .map(({ idx }) => idx);

  console.log('faceRects:', faceRects.objects);
  console.log('confidences:', faceRects.numDetections);
  // look for best result
  const facesImg = this.image.copy();
  faceRects.objects.forEach((rect, i) =>{
    const faceRect = faceRects.objects[sortByNumDetections(faceRects)[i]];
    const eyeResult = this.getEyesLocation(faceRect);
    const eyeRects = sortByNumDetections(eyeResult)
      .slice(0, 2)
      .map(idx => eyeResult.objects[idx]);

      CvUtilities.drawBlueRectangle(facesImg, faceRect);
      eyeRects.forEach(eyeRect => CvUtilities.drawBlueRectangle(facesImg.getRegion(faceRect), eyeRect));
  });


    return facesImg;
};

module.exports = FaceAndEyesDetector
