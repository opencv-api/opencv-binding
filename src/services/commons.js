
exports.runVideoFaceDetection = (src, detectFaces) => CvUtilities.getWebCamFrame(src, 1, (frame) => {
  console.time('detection time');
  const frameResized = frame.resizeToMax(800);

  // detect faces
  const faceRects = detectFaces(frameResized);
  if (faceRects.length) {
    // draw detection
    faceRects.forEach(faceRect => CvUtilities.drawBlueRectangle(frameResized, faceRect));
  }

  opencv.imshow('face detection', frameResized);
  console.timeEnd('detection time');
});
