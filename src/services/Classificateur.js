'use strict';
/**
CvClassifier Class
@constructor
@param {String} title - The description of classifier to be returned
*/
function CvClassifier(title){
  this.title = title;
}

/**
Get classifier
@return {Object} - A opencv CV cascade classifier
*/
CvClassifier.prototype.getClassifier = function () {
  switch (this.title) {
    case "haarFrontalFaceDefault":
    return new opencv.CascadeClassifier(opencv.HAAR_FRONTALFACE_DEFAULT);
    case "haarFrontalFaceAlt2":
    return new opencv.CascadeClassifier(opencv.HAAR_FRONTALFACE_ALT2);
    case "lbpFrontalFace":
    return new opencv.CascadeClassifier(opencv.HAAR_FRONTALFACE_ALT2);
    //lbpFrontalFaceImproved, lbpProfileFace, haarFrontalFaceAlt, haarFrontalFaceAltTree, haarProfileFace, haarSmile
    case "haarEye":
    return new opencv.CascadeClassifier(opencv.HAAR_EYE);
    case "haarEyeTreeEyeGlasses":
    return new opencv.CascadeClassifier(opencv.HAAR_EYE);
    case "haarLeftEye2Splits":
    return new opencv.CascadeClassifier(opencv.HAAR_EYE);
    case "haarRightEye2Splits":
    return new opencv.CascadeClassifier(opencv.HAAR_EYE);

    default:
    return new opencv.CascadeClassifier(opencv.HAAR_FRONTALFACE_DEFAULT);

  }
};

module.exports = CvClassifier
