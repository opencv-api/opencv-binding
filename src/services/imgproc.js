const prefixePng = 'data:image/png;base64,';
const prefixeJpg = 'data:image/jpeg;base64,';

/**
  This function remove the png or jpg conversion
  @param {string} resEnBase64 - a path to an image to be converted

*/
const convertirDuBase64 = (resEnBase64) => {
  const input = resEnBase64.replace(prefixeJpg, '').replace(prefixePng, '');
  const transition = Buffer.from(input, 'base64');
  const output = opencv.imdecode(transition);
  return output;
}

/**
The conversion is possible using the built-in opencv function : imencode
Return a Jpeg version
  @param {image} input - an input image that need to be converted in Jpeg format

*/
const convertirJpgBase64 = (input) => {
  return opencv.imencode('.jpg', input).toString('base64');
}
module.exports = {
  convertirDuBase64,convertirJpgBase64
};
