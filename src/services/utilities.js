'use strict';
/**
  Utilities class
  @constructor
*/
function CvUtilities() {

}

/**
  Draw a rectangle on an Image
  @param {Image} img - An image on which the rectangle will be drawed
  @param {Dimension} rectangle
  @param {Vector} rectColor
  @param {Object} rectProps
*/
CvUtilities.drawRect =function( img, rectangle, rectColor, opts = {  thickness: 2}){
  img.drawRectangle(rectangle, rectColor, opts.thickness, opencv.LINE_8);
};
CvUtilities.drawBlueRectangle= function (img, rectangle, opts = {thickness: 2})  {
  this.drawRect(img, rectangle, new opencv.Vec(0,255,45), opts);
};
CvUtilities.getWebCamFrame = function (portWebCam, delay, onFrame) {
    const videoCapt = new opencv.VideoCapture(portWebCam);
    let finish = false;
    const intervalle = setInterval(()=>{
      let frame = videoCapt.read();
      if(frame.empty){
        videoCapt.reset();
        frame = videoCapt.read();
      }
      onFrame(frame);
      const touchKey = opencv.waitKey(delay);
      finish = key !== -1  && key !== 255;
      if (finish) {
        clearInterval(intervalle);
        console.log("Graciously exiting after user have pressed key! ");
      }
    }, 0);
};

module.exports = CvUtilities
