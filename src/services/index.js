/**
@namespace
*/
global.opencv = require('/usr/lib/node_modules/opencv4nodejs');
global.CvClassifier = require('./Classificateur');
global.CvUtilities = require('./utilities');
global.FaceDetector = require('./facesDetector');
const FaceAndEyesDetector = require('./faceAndEyeDetection');
const FaceRecognizer = require('./faceRecognition')

const {convertirDuBase64, convertirJpgBase64 } = require('./imgproc');



module.exports = {
  convertirJpgBase64,
  convertirDuBase64,
  CvClassifier,
  FaceDetector,
  FaceAndEyesDetector,
  FaceRecognizer
};
