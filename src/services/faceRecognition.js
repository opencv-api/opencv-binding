'use strict';
const fs = require('fs');
const path = require('path');

console.log(opencv.xmodules.face);
if(!opencv.xmodules.face){
  console.log('Server compiled without face module');
}
const classifier = new CvClassifier("haarFrontalFaceAlt2").getClassifier();;
const basePath = './src/data/subjects';
const imgsPath = path.resolve(basePath, 'known');
const nameMappings = ['akon', 'chris-brown', 'kolo', 'obama', 'shah-ruh-khan','salman-khan','usher'];
const imgFiles = fs.readdirSync(imgsPath);

const getFaceImage = (grayImg) => {
  const faceRects = classifier.detectMultiScale(grayImg).objects;
  console.log(faceRects);
  if (!faceRects.length) {
    throw new Error('failed to detect faces');
  }
  return grayImg.getRegion(faceRects[0]);
};

function FaceRecognizer(){
}
FaceRecognizer.prototype.recognizeImages = function () {




  const images = imgFiles
  .map(file => path.resolve(imgsPath, file))
  .map(filePath => opencv.imread(filePath))
  .map(img => img.bgrToGray())
  .map(getFaceImage)
  .map(faceImg => faceImg.resize(80,80));
  const isImageFour = (_, i) => imgFiles[i].includes('4');
  const isNotImageFour = (_, i) => !isImageFour(_, i);
  // use images 1 - 3 for training
  const trainImages = images.filter(isNotImageFour);
  // use images 4 for testing
  const testImages = images.filter(isImageFour);
  const labels = imgFiles
  .filter(isNotImageFour)
  .map(file => nameMappings.findIndex(name => file.includes(name)));

  const runPrediction = (recognizer) => {
    testImages.forEach((img) => {
      const result = recognizer.predict(img);
      console.log('predicted: %s, confidence: %s', nameMappings[result.label], result.confidence);
      //cv.imshowWait('face', img);
      //cv.destroyAllWindows();
    });
  };
  const eigen = new opencv.EigenFaceRecognizer();
  const fisher = new opencv.FisherFaceRecognizer();
  const lbph = new opencv.LBPHFaceRecognizer();
  eigen.train(trainImages, labels);
  fisher.train(trainImages, labels);
  lbph.train(trainImages, labels);

  console.log('eigen:');
  runPrediction(eigen);

  console.log('fisher:');
  runPrediction(fisher);

  console.log('lbph:');
  runPrediction(lbph);
};
FaceRecognizer.prototype.recognizeFromImage = function (twoFacesImg) {

  const trainImgs = imgFiles
    .map(file => path.resolve(imgsPath, file))
    .map(filePath => opencv.imread(filePath))
    .map(img => img.bgrToGray())
    .map(getFaceImage)
    .map(faceImg => faceImg.resize(80, 80));

  const labels = imgFiles
    .map(file => nameMappings.findIndex(name => file.includes(name)));

  const lbph = new opencv.LBPHFaceRecognizer();
  lbph.train(trainImgs, labels);


  const result = classifier.detectMultiScale(twoFacesImg.bgrToGray());

  const minDetections = 10;
  result.objects.forEach((faceRect, i) => {
    if (result.numDetections[i] < minDetections) {
      return;
    }
    const faceImg = twoFacesImg.getRegion(faceRect).bgrToGray();
    const who = nameMappings[lbph.predict(faceImg).label];

    const rect = opencv.drawDetection(
      twoFacesImg,
      faceRect,
      { color: new opencv.Vec(255, 0, 0), segmentFraction: 4 }
    );

    const alpha = 0.4;
    opencv.drawTextBox(
      twoFacesImg,
      new opencv.Point(rect.x, rect.y + rect.height + 10),
      [{ text: who }],
      alpha
    );
  });

  return twoFacesImg;
};
module.exports = FaceRecognizer
