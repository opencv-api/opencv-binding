# Face Recognition API #

NodeJS + OpenCV


### What is this API for? ###

* Facilitate Use Of Face Recognition
* Implementation of OpenCV face recognition module within NodeJS App

### Structure of this Final Year Project on Face Recognition? ###

This projects follow the DevOps rules:

* Bitbucket: Source Code Management & Versionning
* Jenkins: Automatised build and continuous integration
* Sonar: Continuous Code Quality
* Docker: Continuous build in Container *

### Note for Sass ###
To change CSS file every time the Sass file is changed, use the command:
  * sass --watch input.sass:output.css *
  * For more details, read the Sass Documentation:
  https://sass-lang.com/documentation/file.SASS_REFERENCE.html 
   *
